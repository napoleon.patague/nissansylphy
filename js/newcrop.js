
		        
		        var newpic =
		        {
		            imageBox: '#step2 .imageBox',
		            thumbBox: '#step2 .thumbBox',
		            spinner: '#step2 .spinner',
		            imgSrc: ''
		        }

		        var newcropper = new cropbox(newpic);
		        
		        document.querySelector('#step2 #file').addEventListener('change', function(){
		            var reader = new FileReader();
		            reader.onload = function(e) {
		                newpic.imgSrc = e.target.result;
						window.images.newpic = e.target.result;
						$("#step2 .thumbBox").empty()
		                newcropper = new cropbox(newpic);
		            }
		            reader.readAsDataURL(this.files[0]);
		            this.files = [];
		        })
		        document.querySelector('#step2 #btnCropNew').addEventListener('click', function(){
		            var img = newcropper.getDataURL();
					window.nissan.state.cropNewPhoto = true;
		            newpic.imgSrc = img;
					window.images.newpic = img;
		            newcropper = new cropbox(newpic);

		           		jQuery('#tab3').show().siblings().hide();
						//innerTabs[index].classList.remove('active');
						$('#tabs3').addClass('active').siblings().removeClass('active');
		        })
		        document.querySelector('#step2 #btnZoomIn').addEventListener('click', function(){
		            newcropper.zoomIn();
		        })
		        document.querySelector('#step2 #btnZoomOut').addEventListener('click', function(){
		            newcropper.zoomOut();
		        })
