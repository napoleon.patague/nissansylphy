window.onload = function() {
				
		        var oldpic =
		        {
		            imageBox: '#step1 .imageBox',
		            thumbBox: '#step1 .thumbBox',
		            spinner: '#step1 .spinner',
		            imgSrc: ''
		        }

		        var oldcropper = new cropbox(oldpic);
		        
		        document.querySelector('#step1 #file').addEventListener('change', function(){
		            var reader = new FileReader();
		            reader.onload = function(e) {
		                oldpic.imgSrc = e.target.result;
						window.images.oldpic = e.target.result;
						$("#step1 .thumbBox").empty();
		                oldcropper = new cropbox(oldpic);
		            }
		            reader.readAsDataURL(this.files[0]);
		            this.files = [];
		        })
		        document.querySelector('#step1 #btnCropOld').addEventListener('click', function(){
		            var img = oldcropper.getDataURL();

		            oldpic.imgSrc = img;
					window.nissan.state.cropOldPhoto = true;
					window.images.oldpic = img;
		            oldcropper = new cropbox(oldpic);
		            	
		            	//jQuery('#step2').show().siblings().hide();
						
						//$('#step2').addClass('active').siblings().removeClass('active');

		        })
		        document.querySelector('#step1 #btnZoomIn').addEventListener('click', function(){
		            oldcropper.zoomIn();
		        })
		        document.querySelector('#step1 #btnZoomOut').addEventListener('click', function(){
		            oldcropper.zoomOut();
		        })

		        
};