<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
    	<link rel="stylesheet" type="text/css" href="sylphy.css">
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    </head>

    <body>
    		<script src="js/cropbox.js"></script>
    	    <script>
		      window.fbAsyncInit = function() {
		        FB.init({
		          appId      : '1668452993388916',
		          xfbml      : true,
		          version    : 'v2.4'
		        });
		        setTimeout(function(){
				facebookLogin()
				},2000);
		      };

		      window.fbApp = {};
			  function facebookLogin() {
			    console.log('facebook login');
			      FB.getLoginStatus(function(response) {
			      console.log(response);

			      if (response.status === 'connected') {
			        // connected
			        //alert('connected');


			      } else if (response.status === 'not_authorized') {
			        //app not_authorized
			        FB.login(function(response) {
			            if (response && response.status === 'connected') {
			               // alert('connected');
			            }
			        }, { scope: 'email,user_photos,publish_actions' });

			      } else {
			        // not_logged_in to Facebook
			        FB.login(function(response) {
			            if (response && response.status === 'connected') {
			                //alert('connected');
			            }
			        }, { scope: 'email,user_photos,publish_actions' });
			      }
			      }); 
			  }

		      (function(d, s, id){
		         var js, fjs = d.getElementsByTagName(s)[0];
		         if (d.getElementById(id)) {return;}
		         js = d.createElement(s); js.id = id;
		         js.src = "//connect.facebook.net/en_US/sdk.js";
		         fjs.parentNode.insertBefore(js, fjs);
		       }(document, 'script', 'facebook-jssdk'));
		    </script>

		   
	<div id ="container">    
		<div class="header"> 
			<h1>Photo Ads here</h1>
		</div> <br>
		<div class="tabbed round">
			<ul>
				<li id="tabs3">3. Post & Share</li>
				<li id="tabs2">2. Choose New</li>
				<li id="tabs1" class="active">1. Choose Old</li>
			</ul>
		</div>
		
		<form id="" class="form-vertical">

		<div id = "tab1" class="tab active">
			<p>CHOOSE OLD PHOTO</p>
				<div id = "left-panel">
					<div class="form-group col-xs-offset-3">	
						<button type="button" id="chooseGallery" class="btn btn-default btn-md">
				          <span class="glyphicon glyphicon-picture"></span> Choose From Gallery
				        </button>
				    </div>    	
		       

			        <div class="form-group col-xs-offset-3">
				        <button type="button" id="upload" class="btn btn-default btn-md">
				          <span class="glyphicon glyphicon-plus"></span> Upload a Photo
				        </button>
				        <input type='file' id="file" accept="image/gif, image/jpeg, image/png" style="display:none;" />
			    	</div>
		        
			    	<br><br>
		        
			        <div class="form-group col-xs-offset-3">
			        	<label class="control-label">Year Taken </label>
						<div class="form-inline">
							
							<select class="form-control year" required name="year" id="year">
								<option selected disabled value="">Year</option>
										
							</select>
						</div>
					</div>
				</div>
				<div id="right-panel">
					
						<!--<a href="#" class="thumbnail photo" style="width:300px;height:300px" >	   </a> -->

						<div class="imageBox">
					        <div class="thumbBox"></div>
					        <div class="spinner" style="display: none"></div>
					    </div>
					    <div class="action">
				
					        <button type="button" id="btnZoomIn" class="btn btn-default btn-xs" style="float:right;  margin-right:80px">
						        <span class="glyphicon glyphicon-plus"></span>
						    </button>
					         <button type="button" id="btnZoomOut" class="btn btn-default btn-xs" style="float:right;">
						        <span class="glyphicon glyphicon-minus"></span>
						    </button>
					        
					    </div>
					    
					
				</div>

				<div id="bottom-panel">
					<div class="form-group">	
						<div class="col-xs-11">
							<button type="button" id="btnCrop" class="btn btn-primary btn-md pull-right">Crop and Save</button>
							<button type="reset" class="btn btn-default btn-md pull-right">Cancel</button>
						</div>
					</div>
				</div>
			
			
			
		 </div>

		<!-- d*****************************-->

		<div id = "tab2" class="tab">
			<p>CHOOSE NEW PHOTO</p>
				<div id = "left-panel">
					<div class="form-group col-xs-offset-3">	
						<button type="button" class="btn btn-default btn-md">
				          <span class="glyphicon glyphicon-picture"></span> Choose From Gallery
				        </button>
				    </div>    	
		       

			        <div class="form-group col-xs-offset-3">
				        <button type="button" id="upload" class="btn btn-default btn-md">
				          <span class="glyphicon glyphicon-plus"></span> Upload a Photo
				        </button>
				        <input type='file' id="file" accept="image/gif, image/jpeg, image/png" style="display:none;" />
			    	</div>
		        
			    	<br><br>
		        
			        <div class="form-group col-xs-offset-3">
			        	<label class="control-label">Year Taken </label>
						<div class="form-inline">
							
							<select class="form-control year" required name="year" id="year2">
								<option selected disabled value="">Year</option>
										
							</select>
						</div>
					</div>
				</div>
				<div id="right-panel">
					
						<div class="imageBox">
					        <div class="thumbBox"></div>
					        <div class="spinner" style="display: none"></div>
					    </div>
					    <div class="action">
				
					        <button type="button" id="btnZoomIn" class="btn btn-default btn-xs" style="float:right;  margin-right:80px">
						        <span class="glyphicon glyphicon-plus"></span>
						    </button>
					         <button type="button" id="btnZoomOut" class="btn btn-default btn-xs" style="float:right;">
						        <span class="glyphicon glyphicon-minus"></span>
						    </button>
					        
					    </div>
					
				</div>

				<div id="bottom-panel">
						
						<div class="col-xs-11">
							<button type="button" id="btnCrop" class="btn btn-primary btn-md pull-right">Crop and Save</button>
							<button type="button" id="cancel" class="btn btn-default btn-md pull-right">Cancel</button>
						</div>
					
				</div>
			
			<div id = "tab3" class="tab">
				<p>POST & SHARE</p>

				<div id="photo-final">
					<a href="#" class="thumbnail" style="width:300px;height:300px" >	   </a>
				</div>
				<div id="post-panel">
				 <input type="submit" class="btn btn-primary btn-md btn-block" value ="Post">
				</div>
			</div>
			
		</div>

		</form>

		<br><br><br>
		 <div
			  class="fb-like"
			  data-share="true"
			  data-width="450"
			  data-show-faces="true">
		</div>
	</div>
		<script src="js/oldcrop.js"></script>
		<script src="js/newcrop.js"></script>
		<script type="text/javascript">
			/*document.addEventListener("DOMContentLoaded", function() {
				var tabs = document.querySelectorAll('.tabbed li');
				var switchers = document.querySelectorAll('.switcher-box a');
				
				for (var i = 0, len = tabs.length; i < len; i++) {
					tabs[i].addEventListener("click", function() {
						if (this.classList.contains('active'))
							return;
						//var currentAttrValue = jQuery(this).attr('href');
						//jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
						var parent = this.parentNode,
							innerTabs = parent.querySelectorAll('li');

						for (var index = 0, iLen = innerTabs.length; index < iLen; index++) {
							innerTabs[index].classList.remove('active');
						}

						this.classList.add('active');
					});
				}


			});*/


			$(function(){ 

					$('#next1').click(function(){ //select client id		
						jQuery('#tab2').show().siblings().hide();
						//innerTabs[index].classList.remove('active');
						$('#tabs2').addClass('active').siblings().removeClass('active');;
					});

					$('#next2').click(function(){ //select client id		
						jQuery('#tab3').show().siblings().hide();
						//innerTabs[index].classList.remove('active');
						$('#tabs3').addClass('active').siblings().removeClass('active');;
					});

					$('#cancel').click(function(){ //select client id		
						jQuery('#tab1').show().siblings().hide();
						//innerTabs[index].classList.remove('active');
						$('#tabs1').addClass('active').siblings().removeClass('active');;
					});

				
			 });


		    $(function(){
				$('#tab1 #upload').click(function(e){
				    e.preventDefault();
				    $('#tab1 #file').click();}
				);

				$('#tab2 #upload').click(function(e){
				    e.preventDefault();
				    $('#tab2 #file').click();}
				);
			});

			$(function(){
			    var $select = $(".year");
			    
			    for (i=1900;i<=2015;i++){
			        $select.append($('<option></option>').val(i).html(i));
			        
			    }
			});


		</script>
					

    </body>

</html>
